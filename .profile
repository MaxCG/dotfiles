PATH=$HOME/.bin:$PATH
PATH=$HOME/.ghcup/bin:$PATH
PATH=$HOME/.cargo/bin:$PATH

source themeEnv

export TERMINAL=st
export TERMINAL_CLASS="st-256color"

export BROWSER=browser
export BROWSERPROC=vivaldi
export BROWSERCOMMAND=vivaldi-stable
export BROWSERCLASS=Vivaldi-stable

export TERMINAL_SHELL="/bin/fish"
export SHELL="/bin/bash"

export OPENER="xdg-open"
export EDITOR="nvim"

export BEMENU_OPTS="--line-height 20 --fn 'Fira Code Nerd Font Mono Semi-Bold' --tb "#$THEME_PRI" --tf "#$THEME_BG" --fb "#$THEME_BG" --ff "#$THEME_PRI" --hb "#$THEME_PRI" --hf "#$THEME_BG" --hp 8"

# Script variables
export SCRIPT_DIR="$HOME/Documents/myScripts"

# Swallower setup
export PIDSWALLOW_SWALLOW_COMMAND='bspc node $pwid --flag hidden=on'
export PIDSWALLOW_VOMIT_COMMAND='bspc node $pwid --flag hidden=off'

# XDG
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export XDG_STATE_HOME=$HOME/.local/state
export XDG_CACHE_HOME=$HOME/.cache

export GOPATH=$XDG_DATA_HOME/go
export HISTFILE=$XDG_STATE_HOME/bash/history
export IPYTHONDIR=$XDG_DATA_HOME/ipython
export JULIA_DEPOT_PATH=$XDG_DATA_HOME/julia
export LESSHISTFILE=$XDG_CACHE_HOME/less/history

export PATH
