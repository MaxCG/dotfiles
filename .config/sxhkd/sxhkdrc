## wm independent hotkeys

# terminal emulator
super + Return
	$TERMINAL

# Quick open apps
super + {equal}
	bspwmQuickOpen {calc}

super + p
	azpainter

# dmenu
super + {space,o,alt + o}
	menu_{run,search,config}

# make sxhkd reload its configuration files:
super + shift + Escape
	pkill -USR1 -x sxhkd

# Print screen
super{_, + alt} + {F3,F4}
	printScr {cb,_}{full,part}

## Media Keys

XF86MonBrightness{Down,Up}{_, + alt}
	scrBright {dec,inc} {5,1}; scrBright str

XF86KbdBrightness{Down,Up}{_, + alt}
	kbBright {dec,inc} {5,1}; kbBright str

XF86Audio{Raise,Lower}Volume{_, + alt}
	soundCtrl {inc,dec} {5,1}

XF86AudioMute
	soundCtrl mute

XF86Audio{Next,Prev,Play}
	mediaManager {next,prev,playpause}

## bspwm hotkeys

# quit/restart bspwm
super + shift + {_,alt + }q
	bspc {wm -r,quit}

# close and kill
super + {_,alt + }m
	bspc node -{c,k}

# FullScreen
super + {_,alt + }z
	bspc {desktop -l next,node -t \~fullscreen}

# Sticky
super + s
	bspc node -g sticky

# Swap the current node and the biggest node;
super + apostrophe
	bspc node -s biggest.local

# Swap Floating/Tilled
super + f
	bspwmFloating

# Toggle hidden on parent
super + k
	xdo id | pidswallow -t

# focus the node in the given direction
super + {h,n,e,i}
	bspc node -f {west,south,north,east}

# Rezise using bottom and right edges
super + alt + {h,n,e,i}
	bspwmResize {h,j,k,l} 20

# go/send to Desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} 'focused:^{1-9,10}'

# Move between adjacent desktops
super + {u,y}{ + alt,_}
	bspc desktop -f '{prev,next}{_,.occupied}.local'

# Move between monitors
super + {_,shift + }{l,semicolon}
	bspc {monitor -f,node -m} {west,east} --follow

# Move a window
super + shift + {alt + ,_}{h,n,e,i}
	{euclidMover,bspwmMove} {west,south,north,east} 20

# Balance windows
super + {alt + ,_}b
	bspc node @/ -{E,B}
