#! /bin/sh

INTERVAL=15
NULL=/dev/null

debug() {
	tee >(cat 1>&2)
}

poll() {
	iwout=$(iwconfig wlo1)

	ssid=$(echo $iwout | sed 's/.*ESSID:\(\(off\)\|"\(.*\)"\).*/\2\3/g')
	if [[ "$ssid" = "off" ]]; then
		echo '{"connected": false}'
	else
		strenght_nums=$(echo $iwout | sed 's/.*Link Quality=\([0-9]*\)\/\([0-9]*\).*/\1 \2/g')
		read str max_str <<< $strenght_nums
		echo "{\"connected\": \"true\", \"ssid\": \"$ssid\", \"str\": $str, \"maxstr\": $max_str}"
	fi
}

nmlistener() {
	while read -r line; do
		if [[ "$line" =~ state ]]; then
			poll
		fi
	done < <(ip monitor link)
}

regular() {
	while true; do
		poll
		sleep $INTERVAL
	done
}

case $1 in
	"--interval")
		shift
		if [ "$1" -eq "$1" ] > $NULL; then
			INTERVAL=$1
			shift
		else
			echo "Interval is NaN"
			exit
		fi
		;;
	"--test")
		poll
		exit
		;;
esac

nmlistener &
regular &

wait
