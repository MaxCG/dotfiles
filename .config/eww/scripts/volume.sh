#! /bin/sh

poll() {
	output=$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | sed "s/Volume: //")

	read vol muted <<< $output

	[[ "$muted" = "" ]]
	muted=$?

	echo "{\"vol\": $vol, \"is_muted\": $muted}"
}

poll

while read line; do
	if echo $line | grep -q "'change'"; then
		poll
	fi
done < <(pactl subscribe)
