#!/bin/sh

function handleEvent {
	local EVENT_STR=$1
	local EVENT=$(getField -d '>>' 1 <<< $EVENT_STR)


	case $EVENT in
		"workspacev2")
			bttr dump
			;;
	esac
}

HYPRLAND_SOCK=$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock

bttr dump

while read -r line; do
	if [ "$1" = "-d" ]; then
		echo $line;
	else
		handleEvent "$line";
	fi
done < <(socat -U - UNIX-CONNECT:$HYPRLAND_SOCK)
