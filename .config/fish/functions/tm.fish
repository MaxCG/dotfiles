function tm -w tmux
	if [ "$argv" = "" ]
		tmux ls
		read session
		if [ "$session" = "" ]
			command tmux
		else
			command tmux new -As $session
		end
	else
		command tmux $argv
	end
end
