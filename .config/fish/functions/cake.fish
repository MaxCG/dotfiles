function cake
	set -g run "./a.out"
	switch $argv
		case '*.cpp'
			set -g cc "g++ -O2 -std=c++11"
		case '*.c'
			set -g cc "gcc -O2 -Wall -ansi -pedantic"
		case '*.hs'
			set -g cc "ghc -O2 -no-keep-hi-files -no-keep-o-files -dynamic -o a.out"
		case '*.java'
			set -g cc "javac-algs4"
			set tmp (string split -m1 -r '.' $argv)[1]
			set -g run "java-algs4 $tmp"
		case '*'
			echo "Comando de compilador desconhecido"
			return 1
	end

	$cc $argv || return 1
	echo "$argv foi compilado"
	eval $run
end

