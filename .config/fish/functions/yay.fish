function yay --wraps yay
	if  test (count $argv) -eq 1
		switch $argv
			case "-Ss"
				yay -Slq |
				fzf -m --preview 'yay -Qi {1}' |
				yay -S -
				return $status
		end
	end
	command yay $argv
end

