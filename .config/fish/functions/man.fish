function man
	if command man -w $argv > /dev/null
		command man --nj $argv | less
	end
end
