abbr af touch
abbr ad mkcd
abbr c cake
abbr cl clear
abbr e editor
abbr ex exit
abbr g git
abbr s sxiv
abbr z zathura

alias clip "xclip -selection clipboard"
alias grep "grep -in --color"
alias less "editor -p"
alias ls "exa --group-directories-first"
alias lsa "ls -a"
alias mv "mv -i"
alias rbt "cd ~ && reboot"
alias rm "rm -d"
alias rr "lfcd"
alias sdn "cd ~ && shutdown now"
alias hib "systemctl hibernate"
alias susp "systemctl suspend"
alias tmux "tmux -f ~/.config/tmux/tmux.conf"
alias vim "nvim"

set -p fish_function_path ~/.config/fish/prompt

alias cdd 'set -q last_Dir && test -d "$last_Dir" && builtin cd "$last_Dir"'
function set_last_dir --on-event fish_prompt
	set -U last_Dir $PWD
end
cdd

function auto_ls --on-variable PWD
	ls
end
